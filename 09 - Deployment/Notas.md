
## **Notas**

¿ Donde vamos a alojar nuestra aplicacion y datos ?

Amazon S3 : Almacenamiento de objetos creado para recuperar cualquier volumen de datos desde cualquier ubicación

```sh
IMPORTANTE : Visto los videos y el documento de despliegue de la aplicacion podemos ver en tono general que lo que tenemos en Amazon S3 es una maquina virtual ubuntu o del SO que deseemos, y lo que tenemos que hacer es exactamente lo mismo que se hizo en nuestra maquina de desarrollo. Es decir, instalar docker, docker compose, construir (build) las imagenes (containers), migrar los modelos, y correr la app para que funcione el servicio (app). Tal como lo hicimos en nuestra maquina.
```

Notemos que mientras tengamos nuestro shell (donde levantamos los servicios) nuestra app podra recibir peticiones. Si la cerramos los servicios se cerraran. A continuacion los pasos para que la app corra sin tener una terminal abierta.

## **Como dejar corriendo nuestro servicio constantemente**

* Instalacion de la herramienta supervisor.

```sh
sudo apt-get install supervisor
```

* Reiniciamos supervisor.

```sh
service supervisor restart
```

* Nos dirigimos a la siguiente ruta.

```sh
cd /etc/supervisor/conf.d/
```

* Creamos un archivo y completamos los datos de la siguiente forma:

```sh
vim <name_app>.conf
```
En nuestro caso el nombre de la app es 'cride'.
* Datos a completar
```sh
[program:cride]
command=docker-compose -f production.yml up
directory=/home/ubuntu/cride-platzi
redirect_stderr=true
autostart=true
autorestart=true
priority=10
```

* Corremos los siguientes comandos y listo, nuestra app correra siempre independientemente si se cierra nuestra terminal de ejecucion.

```sh
supervisorctl reread
```

```sh
supervisorctl update
```

```sh
supervisorctl start cride
```

```sh
supervisorctl status cride
```
