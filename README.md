# Curso-Django-Avanzado-Platzi

Curso de Django Avanzado en Platzi.

## 01 - Bienvenida

* 01 - ¡Bienvenidos!

## 02 - Cimientos

* 01 - Arquitectura de una aplicación
* 02 - The Twelve-Factor App
* 03 - Codebase Settings modular
* 04 - Codebase Dependencias y archivos de docker
* 05 - Codebase Docker
* 06 - Setups alternativos

## 03 - Modelos

* 01 - Herencia de modelos
* 02 - Proxy models
* 03 - App de usuarios
* 04 - Organizando modelos en un paquete de Django
* 05 - Creando el modelo de perfil de usuario
* 06 - Solución del reto arreglando la migración de users a user
* 07 - Aplicación y modelo de círculos
* 08 - Migraciones y admin de círculos

## 04 - Introducción a Django REST Framework

* 02 - Vistas, URLs y Parsers de DRF 
* 03 - Serializers
* 04 - Buenas prácticas para el diseño de un API REST
* 05 - Request, response, renderers y parsers

## 05 - Real-DRF

* 01 - Autenticación y tipos de autenticación
* 02 - APIView
* 03 - Creando el token de autorización
* 04 - User sign up
* 05 - Limitar login a usuarios con cuenta verificada
* 06 - Configurar envío de email
* 07 - Instalar PyJWT y generar tokens
* 08 - Verificar cuenta usando JWT
* 09 - Actualizar modelo de circle (membership)
* 10 - Crear CircleViewSet
* 11 - Añadiendo autorización y paginación
* 12 - Creación de circulos
* 13 - Update de círculo, custom permissions y DRF Mixins
* 14 - Migración de vistas de usuarios a ViewSets
* 15 - Detalle de usuario
* 16 - Update profile data
* 17 - List members - Recursos anidado
* 18 - Retrieve destroy member
* 19 - Modelo de invitaciones y manager
* 20 - Obtener invitaciones de un miembro
* 21 - Unirse a grupo
* 22 - Filtrado
* 23 - App de rides y modelos
* 24 - Implementar la publicación de un ride
* 25 - Validación de campos de un serializer
* 26 - Listado de rides
* 27 - Editar un ride
* 28 - Unirse a viaje
* 29 - Terminar viaje
* 30 - Calificar viaje

## 06 - Tareas asíncronas

* 01 - Creando tarea asíncrona
* 02 - Creando tarea periódica

## 07 - Testing

* 01 - Python unittest y Django TestCase 
* 02 - DRF APITestCase

## 08 - Django Admin

* 01 - Admin actions Modificar datos de un query
* 02 - Admin actions Regresando una respuesta HTTP

## 09 - Deployment

* 01 - Instalación de la aplicación
* 02 - Configuración del dominio en Mailgun y del Bucket en Amazon S3
* 03 - Configuración final de Docker Container usando Supervisor
* 04 - Tutorial de despliegue de la aplicación (Doc.)
* 05 - Futuros pasos y cierre del curso
