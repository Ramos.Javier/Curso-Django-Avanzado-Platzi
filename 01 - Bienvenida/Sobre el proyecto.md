
---
# **Sobre el proyecto**

## **Comparte Ride**

 Proyecto que permite compartir viejes publicar y realizar viajes en automoviles entre estudiantes.

* Invitacion : Un usuario se une a un grupo 
a traves de una invitacion dentro de cada grupo.

* Ride : Dentro del grupo un miembro puede ofrecer o sumarse a un viaje. Solo un numero definido de usuarios se pueden sumar a un viaje.

* Calificacion : Al finalizar el viaje los usuarios califican su experiencia. Cada usuario tiene una reputacion general y es publica a traves de todos sus circulos.

---
## **Modelo**

* Usuarios
    - Base : 
        - Email
        - Username
        - Phono number
        - First name
        - Last name
    - Perfil : 
        - User (PK)
        - Picture
        - Biography (about me)
        - Rides tomados (total)
        - Rides ofrecidos (total)
    - Miembro :
        - User (PK) / Profile (PK)
        - Circulo (PK)
        - ¿ Es administrador del circulo ? (BOOL)
        - Invitaciones (usadas/restantes)
        - ¿ Quien lo invito ?
        - Rides tomados (dentro del circulo)
        - Rides ofrecidos (dentro del circulo)

* Circulos (grupo)
    - Circulo :
        - Nombre
        - Slug name
        - About
        - Picture
        - Miembro
        - Rides tomados
        - Rides ofrecidos
        - ¿ Es un circulo oficial ?
        - ¿ Es publico ?
        - ¿ Tiene limites de miembros ? ¿ Cual es ?
    -Invitacion:
        - Codigo
        - Circulo (PK)
        - ¿ Quien invita ?
        - ¿ Quien la uso ?
        - ¿ Cuando se uso ?

 * Rides
    - Ride :
        - ¿ Quien lo ofrecio ?
        - ¿ Donde se ofrecio ? (circulo)
        - ¿ Cuando ? ¿ Donde ?
        - Asientos disponibles
        - Comentacios adicionales
        - Calificacion (promedio)
        - ¿ Sigue activo ?
    - Calificacion
        - Ride (PK)
        - Circulo (PK)
        - ¿ Quien lo califica ?
        - ¿ A quien califica ?
        - Calificacion (1-5)

---
## **Funcionalidad**

* Usuarios
    - Sign up (con email de confirmacion)
    - Log in
    - Detalle de usuario
    - Actualizacion de datos
        - Perfil
        - Base

* Circulos
    - Listar todos los circulos (incluye filtrado y busqueda)
    - Crear un circulo
    - Detalle de circulo
    - Actualizar datos
    ---
    - Listar los miembros de un circulo
    - Unirse a un circulo (invitacion)
    - Detalles de un miembro
    - Eliminar a un miembro
    - Obtener codigos de invitacion

* Rides
    - Lista de rides (filtrado y busqueda)
    - Crear un ride en un circulo
    - Unirte a un ride
    ---
    - Calificar un ride
    - Mandar recordatorio a los usuarios para calificar

* Bonus
    - Crear un admin-action que descargue a los usuarios con mejores calificaciones en un CSV.
    - Crear una tarea asincrona que envie reportes mensuales a los admins
    ---
    - Deployment a AWS
    - Configuracion de dominio
    - Setup de certificado SSL

---
## **Los servicios que crearemos con Docker**

A continuacion se haran uso de 4 servicios/contenedores.

* Django - Puerto : 8000
* PostgreSQL - Puerto : 5432
* Redis - Puerto :6379 (Servidor de cache)
* Celery (Flower) - Puerto : 5555 - El cual en realidad esta compuesto por 3 servicios.
    - celeryworker
    - celerybeat
    - flower
    