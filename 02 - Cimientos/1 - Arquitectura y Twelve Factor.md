
---
# **Arquitectura**

* Monolitica
* Distribuida
* Hibrida
* Orientada a Servicios (SOA)

## **Service Oriented Architecture (SOA)**

* Es auto-contenida (self-container) : Las aplicaciones/servicios saben como funcionan, no dependen de ninguna.
* Los servicios son una caja negra para sus consumidores.
* Los servicios representan una actividad de negocio con un fin especifico.

## **Web Service**

No es lo mismo que SOA. Web services es la menera en que se implementa las arquitecturas enfocadas a servicios, es decir se crean estos bloques que son accesibles a traves de la web, son independientes del lenguaje de programacion. Son autocontenidas y son cajas negras. La impelementacion de estos web services suelen seguir estandares muy populares.

* SOAP : Utiliza xml. Provee un protocolo de mensajeria muy claro. Provee un conjunto arbitrario de operaciones a traves del listado de servicios
* RESTFULL HTTP : El objetivo de todo Rest es que nuestras operaciones sean independientes. Depende mas del protocolo HTTP
* GRAPHQL : Funciona como un Querie Lenguage para las API. Es decir un lenguaje de consulta para las APIs. Se trata de escribir los datos y como esos datos pueden mutar y que cosas podemos realizar con ellos. 

---
# **The Twelve-Factor App**

## **Principios**

* Formas **Declarativas** de configuracion.
* Un **Contrato claro** en el OS.
* Listas para **lanzar**.
* Minimizar la **diferencia** entre entornos.
* **Facil de escalar**.

## **Twelve Factor**

* **Codebase** : Una sola fuente de verdad. Una app siempre esta trackeada en un sistema de control de versiones. Ej. Git. No tenes codigo en multiples lados, para development o produccion, es una sola fuente de verdad, todo el codigo se empuja ahi y todo el codigo sale de ahi.

* **Dependencies** : Dependencias EXPLICITAMENTE declaradas e isoladas.

* **Configuration** : Almacena la configuracion en el entorno.

* **Backing services** : Trata a los backing services como recursos conectables. Un backing service, es cualquier servicio que nuestra aplicacion pueda consumir a traves de la red. Por ejemplo, BD, servicios de cola y mensajeria, servicios de envio de email, servicios de cache, etc.

* **Build, release, run** : Separa ESTRICTAMENTE las etapas de construccion y las de ejecucion. Build, construccion convierte nuestro codigo fuente en un paquete. Release, liberacion es la etapa en donde incluimos en el paquete cosas de configuracion como variables de entorno. Run, ejecucion es donde corremos la aplicacion en el entorno en el que se va a ejecutar.

* **Processes** : Ejecuta la app como uno o mas procesos sin estado.

* **Port binding**

* **Concurrency**

* **Disposability**

* **Dev/prod parity** : La diferencia entre produccion y desarrollo debe mantenerse casi NULA.

* **Logs**

* **Admin processes** : El objetivo de Admin Processes es tratar los procesos administrativos como una cosa completamente diferente, no tiene que ver con la app y no tiene que estar con la app. ¿Que hace un admin processes? hace backlog, limpiar datos, etc.
