
---
## **Comandos para trabajar con Docker**

* Crear los contenedores de docker. (Construir las imagenes)
```sh
sudo docker-compose -f local.yml build
```
* Para ver que imagenes construyo
```
sudo docker images
```
* Para correr nuestros servicios 
```sh
sudo docker-compose -f local.yml up
```
* Para detener nuestros servicios
```sh
sudo docker-compose -f local.yml down
```
* Para saber que procesos / imagenes / servicios estan corriendo
```sh
sudo docker-compose -f local.yml ps
```
* Para no estar tediosamente poniendo '-f local.yml'
```sh
export COMPOSE_FILE=local.yml
```
* Luego podemos hacer..
```sh
sudo docker-compose up
sudo docker-compose down
sudo docker-compose ps
```
* Para migrar nuestro modelo al servidor postgresql
```sh
sudo docker-compose -f local.yml run --rm django python manage.py makemigrations
sudo docker-compose -f local.yml run --rm django python manage.py migrate
```

---
## **Comandos de Administracion**

```sh
docker-compose -f local.yml run --rm django python manage.py createsuperuser
```

* Como podemos correr nuestro debugger en django
* Levantamos los servicios
```sh
sudo docker-compose -f local.yml up
```
* Abrimos una nueva terminal, matamos el servicio donde corre django y lo volvemos a levantar.

```sh
sudo docker-compose -f local.yml ps
sudo docker rm -f <name_service_django>
sudo docker-compose -f local.yml run --rm --service-ports django
```
* Para eliminar los registros en la base de datos postgresql
```sh
sudo docker volume rm cride_local_postgres_data
```

* Otros comandos docker
```sh
docker container
docker images
docker volume
docker network
```

---
## **Testing**

* Ejecucion de testings
```sh
sudo docker-compose -f local.yml run --rm django pytest
```

