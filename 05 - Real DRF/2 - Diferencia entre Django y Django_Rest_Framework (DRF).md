
---
# Diferencia entre django y django.rest.framework (DRF)

**Django** es el marco de desarrollo web en python, mientras que **Django Rest Framework** es la biblioteca utilizada en Django para construir API Rest. Django Rest Framework está especialmente diseñado para hacer que las operaciones CRUD sean más fáciles de diseñar en Django.

Puede usar ***Django*** solo para crear una aplicación web completamente funcional sin usar ningún marco de frontend, como React, Angular, etc. Al hacerlo, ha usado Django tanto para el backend como para el frontend. En realidad, al hacer esto, ***no tienes el concepto de backend y frontend***. Su aplicación web es solo su aplicación web, y eso es todo.

Sin embargo, si desea que su interfaz se vea elegante con una decoración CSS compleja, puede considerar el uso de marcos de interfaz (React, Angular). Por supuesto, puede usar Django solo para que su interfaz también se vea elegante, pero debe escribir mucho código para hacerlo, y la plantilla de Django no se usa popularmente en comparación con los marcos de interfaz.

Ahora, supongamos que desea utilizar un marco de frontend. Si no tiene una API REST y su código de interfaz intenta solicitar datos de una de sus URL, obtendrá una cadena de datos o una página HTML como la que obtiene al usar curl, no es útil obtener esos datos para su Interfaz. Sin embargo, si tiene una API REST, sus datos de back-end se serializarán de manera que su código de front-end pueda comprender y deserializar los datos devueltos en algunos objetos de datos como el diccionario que puede usar de inmediato.

Ahora, **Django vs Django Rest Framework**. Puede usar Django solo para hacer API REST, pero debe escribir más código y hacer más diseño. Al usar Django Rest Framework, puede escribir menos código y reutilizarlo mejor.

Además, creo que es posible que desee ver la diferencia entre **API** y **REST API**. Se usan indistintamente, pero no son lo mismo. Por ejemplo, cuando usa Django, está usando las API de Django. La API REST (full), que es solo un tipo de API, se usa para desarrollos web cliente-servidor.

Django Rest Framework facilita el uso de su servidor Django como una API REST.

**REST** (REpresentational State Transfer) significa "transferencia de estado representacional" y API significa interfaz de programación de aplicaciones.

Además, las bibliotecas y las API son similares pero no iguales.
